
@extends('dashboard/pagelayout')
@section('content')
    <div class="header pb-8 pt-5 pt-md-4">
      <div class="container-fluid">
        <div class="header-body">
        
        </div>
      </div>
    </div>

<div class="container-fluid mt--6">
    <div class="row">
        <div class="container-fluid">
          <div class="card">
            <div class="card-header">
              <div class="row align-items-center">
               <div class="col-md-12">
                 <h3>Entry the Stock Type</h3>
                </div>
                <div class="col-md-12">
                    <form action="{{url('/add-stocktype')}}" method="post">
                        {{ csrf_field() }}
                        @include('includes.msg') 
                        <div class="row">
                           <label>Stock-Type</label>
                             <div class="col-md-8 mb-3">
                                <input id="stocktype" type="text" class="form-control" name="stocktype" placeholder="Enter Stock Type" required>
                             </div>
                             <div class="col-md-1">
                               <button type="submit" class="btn btn-primary">Add</button>
                            </div>   
                        </div>
                    </form>
                    <br>
                    <div class="table-responsive">
                       <table id="filter" class="table align-items-center  table-flush">
                        <thead>
                            <tr>
                            <th scope="col" class="text-center">Sl.No</th>
                                <th scope="col">Stock Type</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($stocktype as $type)
                            <tr>
                              <td class="text-center">{{$type->id}}</td>
                              <td>{{$type->name}}</td>
                            
                              <td>
                                <a href="{{url('/stocktype-edit',$type->id)}}">
                                  <i class="fa fa-edit" aria-hidden="true"> </i>
                                </a>
                                <a onclick="return confirm('Are you sure want do Delete Permanently?')" href="{{url('/stocktype-delete',$type->id)}}" class="text-danger">
                                  <i class="fa fa-trash" aria-hidden="true"> </i>
                                </a>
                          </td>
                            </tr>
                            @endforeach 
                          </tbody>
                         </table>               
                          <div class="form-group row mb-2">
                            <div class="col-md-6 offset-md-5">
                              <a class="btn btn-primary" href="{{ url('dashboard') }}"> Back</a>
                            </div>
                          </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>  
@endsection
@section('custom_scripts')
<script>
 $(document).ready(function() {
      var table =  $('#filter').DataTable();
 });
 </script>
 @endsection