
@extends('dashboard/pagelayout')
@section('content')
    <div class="header pb-8 pt-5 pt-md-4">
      <div class="container-fluid">
        <div class="header-body">
        
        </div>
      </div>
    </div>

<div class="container-fluid mt--6">
    <div class="row">
        <div class="container-fluid">
          <div class="card">
            <div class="card-header">
              <div class="row align-items-center">
               <div class="col-md-12">
                 <h3>Edit StockType</h3>
                </div>
                <div class="col-md-12">
                    <form action="{{url('/stocktype-update',$stocktype->id)}}" method="post">
                        {{ csrf_field() }}
                        <div class="row">
                        <label>Stock-Type</label>
                             <div class="col-md-6 mb-3">
                             <input class="form-control" id="stocktype" name="stocktype" value="{{$stocktype->name}}">
                             </div>
                             <div class="col-md-1">
                               <button type="submit" class="btn btn-primary">Update</button>
                            </div>   
                        </div>
                    </form>
                </div>
            </div>
            </div>
        </div>
      </div>  
@endsection