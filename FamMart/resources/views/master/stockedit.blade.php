
@extends('dashboard/pagelayout')
@section('content')
    <div class="header pb-8 pt-5 pt-md-4">
      <div class="container-fluid">
        <div class="header-body">
        
        </div>
      </div>
    </div>

<div class="container-fluid mt--6">
    <div class="row">
        <div class="container-fluid">
          <div class="card">
            <div class="card-header">
              <div class="row align-items-center">
               <div class="col-md-12">
                 <h3>Edit Stock</h3>
                </div>
                <div class="col-md-12">
                    <form action="{{url('/stock-update',$stock->id)}}" method="post">
                        {{ csrf_field() }}
                        <div class="row">
                        <label>Stock-Type</label>
                             <div class="col-md-4 mb-3">
                             <select class="form-control" id="stocktype" name="stocktype">
                                    <option selected disabled>{{$stock->stockType['name']}}</option>
                                    @foreach($stocktype as $type)
                                        <option value="{{$type->id}}">{{$type->name}}</option>
                                    @endforeach
                            </select>
                             </div>
                             <label>Stock Name</label>
                             <div class="col-md-4 mb-3">
                                <input id="stock" type="text" class="form-control" name="stock" value="{{$stock->stock}}" >
                             </div>
                             <div class="col-md-1">
                               <button type="submit" class="btn btn-primary">Update</button>
                            </div>   
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>  
@endsection