
@extends('dashboard/pagelayout')
@section('content')
    <div class="header pb-8 pt-5 pt-md-4">
      <div class="container-fluid">
        <div class="header-body">
        
        </div>
      </div>
    </div>

<div class="container-fluid mt--6">
    <div class="row">
        <div class="container-fluid">
          <div class="card">
            <div class="card-header">
              <div class="row align-items-center">
               <div class="col-md-12">
                 <h3>Entry the Stock</h3>
                </div>
                <div class="col-md-12">
                    <form action="{{url('/add-stock')}}" method="post">
                        {{ csrf_field() }}
                        @include('includes.msg') 
                        <div class="row">
                        <label>Stock-Type</label>
                             <div class="col-md-4 mb-3">
                             <select class="form-control" id="stocktype" name="stocktype" required>
                                    <option value="">Select StockType</option>
                                    @foreach($stocktype as $type)
                                        <option value="{{$type->id}}">{{$type->name}}</option>
                                    @endforeach
                            </select>
                             </div>
                             <label>Stock Name</label>
                             <div class="col-md-4 mb-3">
                                <input id="stock" type="text" class="form-control" name="stock" placeholder="Enter Stock Name" required>
                             </div>
                             <div class="col-md-1">
                               <button type="submit" class="btn btn-primary">Add</button>
                            </div>   
                        </div>
                    </form>
                    <br>
                    <div class="table-responsive">
                       <table id="filter" class="table align-items-center  table-flush">
                        <thead>
                            <tr>
                            <th scope="col" class="text-center">Sl.No</th>
                                <th scope="col">Stock Type</th>
                                <th scope="col">Stock Name</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($stock as $s)
                            <tr>
                            <td class="text-center">{{$s->id}}</td>
                            <td>{{$s->stockType['name']}}</td>
                            <td>{{$s->stock}}</td>             
                            <td>
                                <a href="{{url('stock-edit',[$s->id])}}">
                                <i class="fa fa-edit" aria-hidden="true"> </i>
                                </a>
                                <a onclick="return confirm('Are you sure want do Delete Permanently?')" href="{{url('/stock-delete',[$s->id])}}" class="text-danger">
                                <i class="fa fa-trash" aria-hidden="true"> </i>
                                </a>
                            </td>
                            </tr>
                            @endforeach 
                          </tbody>
                         </table>              
                            <div class="form-group row mb-2">
                            <div class="col-md-6 offset-md-5">
                                <a class="btn btn-primary" href="{{ url('dashboard') }}"> Back</a>
                            </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>  
@endsection
@section('custom_scripts')
<script>
 $(document).ready(function() {
      var table =  $('#filter').DataTable();
 });
 </script>
 @endsection