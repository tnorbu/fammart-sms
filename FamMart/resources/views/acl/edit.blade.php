
@extends('dashboard/pagelayout')
@section('content')
    <div class="header pb-8 pt-5 pt-md-4">
      <div class="container-fluid">
        <div class="header-body">
          <!-- Card stats -->
          <div class="row"> 
          </div>
        </div>
      </div>
    </div>

    <div class="container-fluid mt--5">
      <div class="col">
        <div class="card">
          <div class="card-header">
            <h2>User Edit</h2>
              <div class="col-md-12">
                <div class="card-body">
                </div>
                <form action="{{url('user-update',$users->id)}}" method="post">
                  {{ csrf_field() }}
                  <div class="row">
                    <div class="col-md-3 mb-3">
                      <label>User Name</label>
                      <input id="name" type="text" class="form-control" name="name" value="{{$users->name}}">
                    </div>
                    <div class="col-md-3 mb-3">
                      <label>Email</label>
                      <input id="email" type="text" class="form-control" name="email" value="{{$users->email}}">
                    </div>
                    <div class="col-md-3 mb-3">
                      <label>Password</label>
                      <input id="password" type="password" class="form-control" name="password" value="{{$users->password}}">
                    </div>
                    <div class="col-md-3 mb-3">
                      <label>Comfirm Password</label>
                      <input id="comfirm" type="password" class="form-control" name="comfirm" required>
                    </div>
                  </div>
                  <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                      <button type="submit" class="btn btn-success">Submit</button>
                      <a class="btn btn-primary" href="{{ url('user-list') }}"> Back</a>
                    </div>
                  </div></br>
                </form>   
              </div>
            </div>
          </div>  
@endsection