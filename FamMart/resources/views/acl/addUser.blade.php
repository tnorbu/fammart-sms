

@extends('dashboard/pagelayout')
@section('content')

    <div class="header pb-8 pt-5 pt-md-4">
      <div class="container-fluid">
        <div class="header-body">
          <!-- Card stats -->
            <div class="row"> 
            </div>
        </div>
      </div>
    </div>
    
    <div class="container-fluid mt--6">
        <div class="row">
           <div class="container-fluid">
               <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                               <h2>Add User</h2></hr>
                            </div>
                                <div class="col-md-12">
                                    <div class="card-body">
                                        <form action="{{url('user-add')}}" method="post">
                                            {{ csrf_field() }}
                                            @include('includes.msg') 
                                            <div class="row">
                                                <div class="col-md-3 mb-3">
                                                    <label>User Name</label>
                                                    <input id="name" type="text" class="form-control" name="name" placeholder="Name" required>
                                                </div>
                                                <div class="col-md-3 mb-3">
                                                    <label>Email</label>
                                                    <input id="email" type="text" class="form-control" name="email" placeholder="Email address" required>
                                                </div>
                                                <div class="col-md-3 mb-3">
                                                    <label>Password</label>
                                                    <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>
                                                </div>
                                                <div class="col-md-3 mb-3">
                                                    <label>Confirm Password</label>
                                                    <input id="confirm" type="password" class="form-control" name="confirm"placeholder="Confirm Password" required>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-0">
                                                <div class="col-md-6 offset-md-4">
                                                    <button type="submit" class="btn btn-success">
                                                        {{ __('Submit') }}
                                                    </button>
                                                    <a class="btn btn-primary" href="{{ url('user-list') }}"> Back</a>
                                                </div>
                                            </div>
                                        </form> 
                                   
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
@endsection