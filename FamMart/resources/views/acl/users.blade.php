
@extends('dashboard/pagelayout')
@section('content')
    <div class="header pb-7 pt-5 pt-md-4">
      <div class="container-fluid">
        <div class="header-body">
          <!-- Card stats -->
            <div class="row"> 
            </div>
        </div>
      </div>
    </div>

      <div class="container-fluid mt--5">
        <div class="col">
          <div class="card">
            <div class="card-header">
              <h2>User Lists</h2>
                  <a class="btn-sm btn-primary" href="{{url('/add-user')}}">Add User</a>
            </div>
            @include('includes.msg') 
            <div class="table-responsive">
              <table class="table align-items-center table-flush">
                <thead>
                  <tr>
                    <th scope="col" class="text-center">Sl.No</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                  <tbody> 
                    @foreach($users as $u)
                      <tr>
                        <td class="text-center">{{$u->id}}</td>
                          <td>{{$u->name}}</td>   
                          <td>{{$u->email}}</td>  
                          <td>
                            <a href="{{url('user-edit',$u->id)}}">
                              <i class="fa fa-edit" aria-hidden="true"> </i>
                            </a>
                            <a onclick="return confirm('Are you sure want do Delete Permanently?')" href="{{url('user-delete',$u->id)}}" class="text-danger">
                              <i class="fa fa-trash" aria-hidden="true"> </i>
                            </a>              
                          </td>
                      </tr>
                    @endforeach
                  </tbody>
              </table>
              <br>             
                <div class="form-group row mb-2">
                  <div class="col-md-6 offset-md-5">
                    <a class="btn btn-primary" href="{{ url('dashboard') }}"> Back</a>
                  </div>
                </div>                 
            </div>
          </div>
@endsection