
@extends('dashboard/pagelayout')
@section('content')
<div class="header pb-7 pt-5 pt-md-4">
   <div class="container-fluid">
      <div class="header-body">
         <!-- Card stats -->
         <div class="row"> 
         </div>
      </div>
   </div>
</div>

    <div class="container-fluid mt--5">
        <div class="col">
          <div class="card">
            <div class="card-header">
              <h3>Credit Lists</h3>
            </div>
            @include('includes.msg') 
        </div>
          <div class="card">
            <div class="card-header">
            <div class="table-responsive">
              <table id="filter" class="table align-items-center table-flush">
                <thead>
                  <tr>
                    <th scope="col" class="text-center">Sl.No</th>
                    <th scope="col">Date</th>
                    <th scope="col">Name</th>
                    <th scope="col">Stock Name</th>
                    <th scope="col">Quantity</th>
                    <th scope="col">Rate</th>
                    <th scope="col">Total Amount</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                  <tbody>
                  @foreach($creditlist as $i=>$d) 
                      <tr>
                         <td class="text-center">{{$i+1}}</td>
                         <td>{{$d->date}}</td>    
                          <td>{{$d->name}}</td>    
                          <td>{{$d->stock_id}}</td> 
                          <td>{{$d->quantity}}</td>    
                          <td>{{$d->sale_rate}}</td>     
                          <td>{{$d->total_amount}}</td> 
                          <td>
                          <a href="{{url('credit-view',$d->id)}}">
                              <i class="fa fa-eye" aria-hidden="true"></i>
                            </a>
                            <a onclick="return confirm('Are you sure want do Delete Permanently?')" href="{{url('credit-delete',$d->id)}}" class="text-danger">
                              <i class="fa fa-trash" aria-hidden="true"> </i>
                            </a>
                          </td>
                    </tr>
                    @endforeach
                   </tbody>
                 </table>              
                  <div class="form-group row mb-2">
                      <div class="col-md-6 offset-md-5">
                            <a class="btn btn-primary" href="{{ url('daily-saleRecord') }}"> Back</a>
                      </div>
                  </div>
                </div>
              </div>
            </div>
@endsection
@section('custom_scripts')
<script>
 $(document).ready(function() {
      var table =  $('#filter').DataTable();
 });
 </script>
 @endsection