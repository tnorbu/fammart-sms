@extends('dashboard/pagelayout')
@section('content')
    <div class="header pb-7 pt-5 pt-md-4">
      <div class="container-fluid">
        <div class="header-body">
          <!-- Card stats -->
            <div class="row"> 
            </div>
        </div>
      </div>
    </div>
      <!-- Dark table -->
      <div class="container-fluid mt--5">
        <div class="col">
        <div class="container-fluid">
          <div class="card">
            <div class="card-header">
              <h3>Daily Sale Record</h3>
              @include('includes.msg') 
              <div class="row">
                <div class="col-md-8">
                <input id="date" type="date" class="form-control" name="date"><br>           
                </div>
                <div class="form-group row mb-1">
                  <div class="col-md-12 offset-md-4">
                     <a class="btn btn-primary" href="{{ url('/add-dailySale') }}">Add Daily Sale</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header">
            <div class="table-responsive">
              <table id="filter" class="table align-items-center table-flush">
                <thead>
                  <tr>
                   <th scope="col" class="text-center">Sl.No</th>
                    <th scope="col">Date</th>
                    <th scope="col">Particulars</th>
                    <th scope="col">Total Amount</th>
                    <th scope="col">Status</th>
                    <th scope="col">TakenBy</th>
                    <th scope="col">Remark</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                @foreach($dailysale as $i=>$d)
                  <tr>
                        <td class="text-center">{{$i+1}}</td>
                          <td>{{$d->date}}</td>   
                          <td>{{$d->particular}}</td> 
                          <td>{{$d->amount}}</td>
                          <td>{{$d->paymentStatus['name']}}</td>
                          <td>{{$d->takenBy}}</td>  
                          <td>{{$d->remarks}}</td>  
                    
                          <td>
                            <a onclick="return confirm('Are you sure want do Delete Permanently?')" href="{{url('saleRecord-delete',$d->id)}}" class="text-danger">
                              <i class="fa fa-trash" aria-hidden="true"> </i>
                            </a>
                          </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>              
                <div class="form-group row mb-2">
                  <div class="col-md-6 offset-md-5">
                    <a class="btn btn-primary" href="{{ url('dashboard') }}"> Back</a>
                  </div>
                </div>
            </div>
          </div>
        </div>

@endsection
@section('custom_scripts')
<script>
 $(document).ready(function() {
      var table =  $('#filter').DataTable();
        $('#date').on('change', function () {
            table.columns(1).search( this.value ).draw();
        });
 });
 </script>
 @endsection
     