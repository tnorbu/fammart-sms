
@extends('dashboard/pagelayout')
@section('content')
    <div class="header pb-8 pt-5 pt-md-4">
      <div class="container-fluid">
        <div class="header-body">
        
        </div>
      </div>
    </div>

<div class="container-fluid mt--6">
    <div class="row">
        <div class="container-fluid">
          <div class="card">
            <div class="card-header">
              <div class="row align-items-center">
               <div class="col-md-12">
                 <h3>View Debit Details </h3>
                </div>
                <div class="col-md-12">
                  <div class="card-body">
                    <div>
                        <div class="row">
                            <div class="col-md-4 mb-3">
                            <label>Date</label>
                                <input id="date" type="date" class="form-control" name="date" readonly value="{{$debitlist->date}}">
                            </div>
                            <div class="col-md-4 mb-3">
                            <label>Particulars</label>
                                <input id="particular" type="text" class="form-control" name="particular"readonly value="{{$debitlist->stock_id}}">
                            </div>
                            <div class="col-md-4 mb-3"> 
                              <label>Quantity</label>
                              <div class="input-group">
                              <input id="quantity" type="text" class="form-control" name="quantity" readonly value="{{$debitlist->quantity}}">
                                 <div class="input-group-prepend">
                                   <span class="input-group-text">CTN</span>
                                  </div>
                                </div>
                            </div>  
                          </div>                      
                          <div class="row">
                            <div class="col-md-3 mb-3">
                            <label>Total Amount</label>                            
                               <div class="input-group">
                                     <div class="input-group-prepend">
                                       <span class="input-group-text">Nu.</span>
                                     </div>
                                     <input id="total_amount" type="text" class="form-control" name="total_amount" readonly value="{{$debitlist->total_amount}}">
                                 </div>
                             </div>
                            <div class="col-md-3 mb-3">
                            <label>TakenBy</label>
                                <input id="name" type="text" class="form-control" name="name" readonly value="{{$debitlist->name}}">
                            </div>
                            <div class="col-md-6 mb-3">
                            <label>Remarks</label>
                                <textarea id="remark" type="text" class="form-control" name="remark" readonly>{{$debitlist->remarks}}</textarea>
                            </div>
                        </div>
                      
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <a class="btn btn-primary" href="{{ url('debit-list') }}"> Back</a>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>  
@endsection