
@extends('dashboard/pagelayout')
@section('content')
    <div class="header pb-8 pt-5 pt-md-4">
      <div class="container-fluid">
        <div class="header-body">
        
        </div>
      </div>
    </div>

<div class="container-fluid mt--6">
    <div class="row">
        <div class="container-fluid">
          <div class="card">
            <div class="card-header">
              <div class="row align-items-center">
               <div class="col-md-12">
                 <h3>Entry the Daily Sale Details </h3>
                </div>
                <div class="col-md-12">
                  <div class="card-body">
                    <div>
                    </div>
                    <form action="{{url('/insert-dailysale')}}" method="post">
                        {{ csrf_field() }}
                        @include('includes.msg') 

                        <div class="row">
                            <div class="col-md-4 mb-3">
                            <label>Date</label>
                                <input id="date" type="date" class="form-control" name="date"required>
                            </div>
                            <div class="col-md-4 mb-3">
                            <label>Particulars</label>
                                <input id="particular" type="text" class="form-control" name="particular" placeholder="Particulars" required>
                            </div>
                            <div class="col-md-4 mb-3"> 
                            <label>Total Amount</label>                            
                               <div class="input-group">
                                     <div class="input-group-prepend">
                                       <span class="input-group-text">Nu.</span>
                                     </div>
                                     <input id="total_amount" type="number" class="form-control" name="total_amount" placeholder="Amount" required>
                                 </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-3 mb-3">
                            <label>Status</label>
                            <select class="form-control" id="status" name="status" required>
                                <option value="">Select Status</option>
                                @foreach($status as $p)
                                    <option value="{{$p->id}}">{{$p->name}}</option>
                                @endforeach
                            </select>
                            </div>
                            <div class="col-md-3 mb-3">
                            <label>TakenBy</label>
                                <input id="name" type="text" class="form-control" name="name" placeholder="Name" required>
                            </div>
                            <div class="col-md-6 mb-3">
                            <label>Remarks</label>
                                <textarea id="remark" type="text" class="form-control" name="remark" placeholder="Remarks" required></textarea>
                            </div>
                        </div>
                      
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-success">
                                    {{ __('Submit') }}
                                </button>
                                <a class="btn btn-primary" href="{{ url('daily-saleRecord') }}"> Back</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>  
@endsection