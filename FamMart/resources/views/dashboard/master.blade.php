
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>
    Dashboard|FamMart
  </title>
  <!-- Favicon ,Fonts,icons and CSS -->

  @include('includes/css')

</head>

<body class="">
  <nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
   @include('includes/topheader') 
        <!-- Navigation -->
       
    @include('includes/sidebar')
          
  <div class="main-content">
    <!-- Navbar -->
    
    @include('includes/navbar')

    <!-- End Navbar -->
    <!-- Header -->
       <!-- @include('includes/header')  -->

      @yield('content')
      
      </div>
      <!-- Footer -->
      @include('includes/footer')

   </div>
  </div>
  <!--   Core, JS and ARGON JS Scripts -->
  @include('includes/scripts')
  <script>
    window.TrackJS &&
      TrackJS.install({
        token: "ee6fab19c5a04ac1a32a645abde4613a",
        application: "argon-dashboard-free"
      });
  </script>
</body>

</html>