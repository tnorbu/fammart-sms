@extends('dashboard/pagelayout')
@section('content')
    <div class="header pb-7 pt-5 pt-md-4">
      <div class="container-fluid">
        <div class="header-body">
         
        </div>
      </div>
    </div>
      <!-- Dark table -->
      <div class="container-fluid mt--5">
        <div class="col">
          <div class="card">
            <div class="card-header">
              <h3>Stock Updated Lists</h3>
              @include('includes.msg') 
              <div class="row">
                <div class="col-md-8">
                <input id="date" type="date" class="form-control" name="date"> <br>         
                </div>
                <div class="form-group row mb-1">
                  <div class="col-md-12 offset-md-4">
                     <a class="btn btn-primary" href="{{ url('/stock-entry') }}"> Add Stock</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header">
            <div class="table-responsive">
              <table id="filter" class="table align-items-center  table-flush border-collapse">
                <thead>
                  <tr>
                   <th scope="col" class="text-center">Sl.No</th>
                    <th scope="col">Date</th>
                    <th scope="col">Suppilier Name</th>
                    <th scope="col">Stock Name</th>
                    <th scope="col">Quantity</th>
                    <th scope="col">Landing Rate</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                @foreach($stockrecords as $i=>$stock)
                  <tr>
                    <td class="text-center">{{$i+1}}</td>
                    <td>{{$stock->date}}</td>
                    <td>{{$stock->supplier_name}}</td>
                    <td>{{$stock->stock['stock']}}</td>
                    <td>{{$stock->quantity}}</td>
                    <td>{{$stock->rate}}</td>
                    <td>
                      <a href="{{url('/stock-view',$stock->id)}}">
                         <i class="fa fa-eye" aria-hidden="true"></i>
                      </a>
                      <a href="{{url('/stockrecord-edit',$stock->id)}}">
                         <i class="fa fa-edit" aria-hidden="true"> </i>
                      </a>
                      <a onclick="return confirm('Are you sure want do Delete Permanently?')" href="{{url('/stockrecord-delete',$stock->id)}}" class="text-danger">
                        <i class="fa fa-trash" aria-hidden="true"> </i>
                      </a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>               
                <div class="form-group row mb-2">
                  <div class="col-md-6 offset-md-5">
                    <a class="btn btn-primary" href="{{ url('dashboard') }}"> Back</a>
                  </div>
                </div>
            </div>
          </div>
        </div>

@endsection
@section('custom_scripts')
<script>
 $(document).ready(function() {
      var table =  $('#filter').DataTable();
        $('#date').on('change', function () {
            table.columns(1).search( this.value ).draw();
        });
 });
 </script>
 @endsection

     