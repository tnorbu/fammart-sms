
@extends('dashboard/pagelayout')
@section('content')
    <div class="header pb-8 pt-5 pt-md-4">
      <div class="container-fluid">
        <div class="header-body">
          <!-- Card stats -->
            <div class="row"> 
            </div>
        </div>
      </div>
    </div>

    <div class="container-fluid mt--6">
      <div class="row">
        <div class="container-fluid">
          <div class="card">
            <div class="card-header">
              <div class="row align-items-center">
               <div class="col-md-12">
                 <div class="py-2 text-center">
                   <h2>Stock Update Details</h2>
                 </div>
                </div>
                <div class="col-md-12">
                  <div class="card-body">
                    <div>
                    <form action="{{url('update-stock',$stock->id)}}" method="POST">
                     {{ csrf_field() }}
                     @include('includes.msg') 
                        <div class="row">
                            <div class="col-md-4 mb-3">
                            <label >Supplier Name</label>
                             <input type="text" class="form-control" name="name" id ="name" readonly value="{{$stock->supplier_name}}">
                            </div>
                            <div class="col-md-4 mb-3">
                            <label >Submitted Date</label>
                             <input type="text" class="form-control" name="date" id ="date" readonly value="{{$stock->date}}">
                            </div>
                            <div class="col-md-4 mb-3">
                            <label >Stock Type</label>
                             <!-- <input type="text" class="form-control" name="type" id ="type" readonly value="{{$stock->stockType['name']}}"> -->
                             <select class="form-control" id="stocktype" name="stocktype">
                          <option readonly value="{{$stock->stockType['id']}}">{{$stock->stockType['name']}}</option>
                            @foreach($stock_types as $type)
                            <option value="{{$type->id}}">{{$type->name}}</option>
                           @endforeach
                        </select>

                            </div>
                        </div>
                        <div class="row">
                        <input type="hidden" id="stockno" name="stockno" value="{{$stock->stockNumber}}"/>
                            <div class="col-md-4 mb-3">
                            <label >Stock Name</label>
                              <input type="text" class="form-control" name="stock" id ="stock" readonly value="{{$stock->stock['stock']}}">
                            </div>
                            <div class="col-md-4 mb-3">
                            <label>Price</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Nu.</span>
                                </div>
                                <input type="number" class="form-control" name="price" id ="price" readonly value={{$stock->rate}}>
                            </div>
                            </div>
                            <div class="col-md-4 mb-3">
                            <label>Status</label>
                            <div class="input-group">
                            <select class="form-control" id="status" name="status" readonly>
                                    <option required value="T" {{($stock->status == 'T') ? 'selected' : '' }}>
                                    Transaction</option>
                                </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 mb-3">
                            <label>Taken By</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="name" id ="name" placeholder="Name" required>
                            </div>
                            </div>
                            <div class="col-md-4 mb-3">
                            <label>Taken Quantity</label>
                                <input type="hidden" id="history" name="history" value="{{$stock->quantity}}">
                                <div class="input-group">
                                <input type="number" class="form-control" name="quantity" id ="quantity" value="{{$stock->quantity}}" onchange="calculateAmount(this.value)" required>
                                    <div class="input-group-prepend">
                                    <span class="input-group-text">CTN</span>
                                    </div>
                                </div>
                            </div> 
                            <div class="col-md-4 mb-3">
                            <label>Sold Price</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Nu.</span>
                                </div>
                                <input type="number" class="form-control" name="rate" id ="rate" placeholder="Price per ctn" onchange="calculateAmount(this.value)" required>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-3 mb-3">
                            <label>Total_Amount</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Nu.</span>
                                </div>
                                <input type="number" class="form-control" name="total_amount" id ="total_amount" placeholder="Total Amount" readonly>
                            </div>
                          </div>
                          <div class="col-md-3 mb-3">
                            <label>Status</label>
                            <select class="form-control" id="p_status" name="p_status" required>
                                <option value="">Select Status</option>
                                @foreach($status as $p)
                                    <option value="{{$p->id}}">{{$p->name}}</option>
                                @endforeach
                            </select> 
                            </div>
                          <div class="col-md-6 mb-3">
                            <label>Remarks</label>
                            <textarea class="form-control" id="remark" name="remark" cols="50" rows="2" required>{{$stock->remarks}}</textarea>
                          </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-success">
                                    {{ __('Update') }}
                                </button>
                                <a class="btn btn-primary" href="{{ url('stock-record') }}"> Back</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
function calculateAmount(val) {
        var quantity = document.getElementById("quantity").value;
        var price = document.getElementById("rate").value;

           var tot_amount = quantity * price;
            var amount = document.getElementById('total_amount');
            amount.value = tot_amount;
         }
</script>
@endsection