@extends('dashboard/pagelayout')
@section('content')
<div class="header pb-8 pt-5 pt-md-4">
  <div class="container-fluid">
    <div class="header-body">   
    </div>
  </div>
</div>

<div class="container-fluid mt--6">
  <div class="row">
    <div class="container-fluid">
      <div class="card">
        <div class="card-header">
          <div class="row align-items-center">
          <div class="col-md-12">
                 <h3>Entry the Stock Details </h3><hr>
                </div>
              <div class="col-md-12 order-md-1">
              <form action="{{url('/stock-insert')}}" method="post">
                        {{ csrf_field() }}
                 @include('includes.msg') 
                  <div class="row">
                    <div class="col-md-3 mb-3">
                      <label>Suppilier Name</label>
                        <input id="supplier_name" type="text" class="form-control" name="supplier_name" placeholder="Enter Name" required>
                    </div>
                    <div class="col-md-3 mb-3">
                      <label>Stock Type</label>
                        <select class="form-control" id="stocktype" name="stocktype" required>
                          <option value="">Select StockType</option>
                            @foreach($stock_types as $type)
                            <option value="{{$type->id}}">{{$type->name}}</option>
                           @endforeach
                        </select>
                    </div>
                    <div class="col-md-3 mb-3">
                      <label>Stock Name</label>
                      <select class="form-control" id="stock" name="stock" required>
                      <option value="">Choose...</option>
                      </select>
                    </div>

                    <div class="col-md-3 mb-3">
                      <div class="form-group">
                        <label>Quantity</label>   
                        <div class="input-group">
                          <input id="quantity" type="number" class="form-control" name="quantity" placeholder="Total Quantity" onchange="calculateAmount(this.value)"  required>
                            <div class="input-group-prepend">
                              <span class="input-group-text">CTN</span>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-3 mb-3">
                      <div class="form-group">
                      <label>Rate(per CTN)</label>
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Nu.</span>
                          </div>
                            <input id="rate" type="number" class="form-control" name="rate" placeholder="Rate" onchange="calculateAmount(this.value)" required>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-3 mb-3">
                      <label>Total Amount</label>  
                      <div class="form-group">                          
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Nu.</span>
                          </div>
                          <input type='number' id="total_amount"  class="form-control" name="total_amount" placeholder="Total Amount" readonly>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6 mb-3">
                      <label>Remarks</label>                            
                        <textarea id="remark" type="text" class="form-control" name="remark" placeholder="Remarks" required></textarea><br>
                    </div>              
                  </div> 
                    <div class="form-group row mb-0">
                      <div class="col-md-6 offset-md-4">
                      <button type="submit" class="btn btn-success">{{ __('Submit') }}</button>
                      <a class="btn btn-primary" href="{{ url('/stock-record') }}"> Back</a> 
                      </div>
                    </div>     
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script type="text/javascript">
          $(window).on('load', function() {
        console.log('All assets are loaded')
    })

    $(document).ready(function () {
      $("#stocktype").on('change',function(e){
            console.log(e);
            var id = e.target.value;
            //alert(id);
            $.get('/json-stock-type?stock_types=' + id, function(data){
                console.log(data);
                $('#stock').empty();
                $('#stock').append('<option value="">Select Stock</option>');
                $.each(data, function(index, ageproductObj){
                    $('#stock').append('<option value="'+ ageproductObj.id +'">'+ ageproductObj.stock + '</option>');
                })
            });
        });
    });
  
    function deletFn() {
      if (confirm('Are you sure you want Delete Permanently?'))  {
        $.ajax({
          type: "POST",
          url: url,
          success: function(result) {
            location.reload();
          }
        });
      }
    }  
      function calculateAmount(val) {
        var quantity = document.getElementById("quantity").value;
        var price = document.getElementById("rate").value;

           var tot_amount = quantity * price;
            var amount = document.getElementById('total_amount');
            amount.value = tot_amount;
            }
</script>
@endsection








