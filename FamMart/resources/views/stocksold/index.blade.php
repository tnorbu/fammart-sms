@extends('dashboard/pagelayout')
@section('content')
    <div class="header pb-7 pt-5 pt-md-4">
      <div class="container-fluid">
        <div class="header-body">
          <!-- Card stats -->
            <div class="row"> 
            </div>
        </div>
      </div>
    </div>
      <!-- Dark table -->
      <div class="container-fluid mt--5">
        <div class="col">
          <div class="card">
            <div class="card-header">
              <h3>Stock Sold Lists</h3>
              @include('includes.msg') 
              <div class="row">
                <div class="col-md-8">
                <input id="date" type="date" class="form-control" name="date">           
                </div><br>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header">
            <div class="table-responsive">
              <table id="filter" class="table align-items-center  table-flush">
                <thead>
                  <tr>
                   <th scope="col" class="text-center">Sl.No</th>
                    <th scope="col">Date</th>
                    <th scope="col">TakenBy</th>
                    <th scope="col">StockName</th>
                    <th scope="col">Quantity</th>
                    <th scope="col">Rate</th>
                    <th scope="col">TotalAmount</th>
                    <th scope="col">SoldBy</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
               @foreach($stockhistories as $i=>$stock)
                  <tr>
                    <td class="text-center">{{$i+1}}</td>
                    <td>{{$stock->date}}</td>
                    <td>{{$stock->name}}</td>
                    <td>{{$stock->stock_id}}</td>
                    <td>{{$stock->quantity}}</td>
                    <td>{{$stock->sale_rate}}</td>
                    <td>{{$stock->total_amount}}</td>
                    <td>{{$stock->submitted_by}}</td>
                    <td>
                    <a onclick="return confirm('Are you sure want do Delete Permanently?')" href="{{url('/stocksold-delete',$stock->id)}}" class="text-danger">
                        <i class="fa fa-trash" aria-hidden="true"> </i>
                      </a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>               
                <div class="form-group row mb-2">
                  <div class="col-md-6 offset-md-5">
                    <a class="btn btn-primary" href="{{ url('dashboard') }}"> Back</a>
                  </div>
                </div>
            </div>
          </div>
        </div>

@endsection
@section('custom_scripts')
<script>
 $(document).ready(function() {
      var table =  $('#filter').DataTable();
        $('#date').on('change', function () {
            table.columns(1).search( this.value ).draw();
        });
 });
 </script>
 @endsection
     