
@extends('dashboard/pagelayout')
@section('content')
    <div class="header pb-8 pt-5 pt-md-4">
      <div class="container-fluid">
        <div class="header-body">
          <!-- Card stats -->
            <div class="row"> 
            </div>
        </div>
      </div>
    </div>

    <div class="container-fluid mt--6">
      <div class="row">
        <div class="container-fluid">
          <div class="card">
            <div class="card-header">
              <div class="row align-items-center">
               <div class="col-md-12">
                 <h3>Bill Edit
                </div>
                <div class="col-md-12">
                  <div class="card-body">
                    <div>
                    <form action="{{url('/bill-update',$bill->id)}}" method="post">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-3 mb-3">
                            <label>Date</label>
                                <input id="date" type="date" class="form-control" name="date" readonly value="{{$bill->date}}">
                            </div>
                            <div class="col-md-3 mb-3">
                               <label>Bill Name</label>
                                <input id="name" type="text" class="form-control" name="name"readonly value="{{$bill->billName}}">
                            </div>
                            <div class="col-md-3 mb-3">
                            <label>Bill Type</label>
                                <input id="bill" type="text" class="form-control" name="bill" readonly value="{{$bill->billType['name']}}">
                            </div>
                            <div class="col-md-3 mb-3">
                            <label>Status</label>
                            <div class="input-group">
                            <select class="form-control" id="billTrans" name="billTrans" readonly>
                                    <option required value="T" {{($bill->billTrans == 'T') ? 'selected' : '' }}>
                                    Transaction</option>
                                </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                        <div class="col-md-3 mb-3">
                            <label>Status</label>
                            <select class="form-control" id="status" name="status" required>
                                <option selected disabled>{{$bill->paymentStatus['name']}}</option>
                                @foreach($status as $p)
                                    <option value="{{$p->id}}">{{$p->name}}</option>
                                @endforeach
                            </select> 
                            </div>
                        <div class="col-md-3 mb-3">
                            <label>Previous Amount</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                     <span class="input-group-text">Nu.</span>
                                </div>
                                    <input id="amount" type="number" class="form-control" name="amount" value="{{$bill->totalAmount}}" required>
                            </div>                            
                            </div>
                            <div class="col-md-3 mb-3">
                            <label>Today Debit</label>
                            <div class="input-group">
                            <div class="input-group-prepend">
                                     <span class="input-group-text">Nu.</span>
                                </div>
                                <input id="debit" type="text" class="form-control" name="debit" onchange="calculate(this.value)" required>
                            </div>
                            </div>
                            <div class="col-md-3 mb-3">
                            <label>Amount Balance</label>
                            <div class="input-group">
                            <div class="input-group-prepend">
                                     <span class="input-group-text">Nu.</span>
                                </div>
                                <input id="balance" type="text" class="form-control" name="balance">
                            </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 mb-3">
                            <label>Today Credit</label>
                            <div class="input-group">
                            <div class="input-group-prepend">
                                     <span class="input-group-text">Nu.</span>
                                </div>
                                <input id="credit" type="text" class="form-control" name="credit" onchange="calculateAmount(this.value)" required>
                            </div>
                         </div>
                         <div class="col-md-3 mb-3">
                            <label>Total Balance(Due)</label>
                            <div class="input-group">
                            <div class="input-group-prepend">
                                     <span class="input-group-text">Nu.</span>
                                </div>
                                <input id="totalamount" type="text" class="form-control" name="totalamount">
                            </div>
                            </div>
                            <div class="col-md-6 mb-3">
                            <label>Remarks</label>
                                <textarea class="form-control" id="remark" name="remark">{{$bill->remarks}}</textarea>
                            </div>
                        </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-success">
                                    {{ __('Update') }}
                                </button>
                                <a class="btn btn-primary" href="{{ url('bill-list') }}"> Back</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
function calculate(val) {
        var amount = document.getElementById("amount");
        var debit = document.getElementById("debit").value;

           var tot_balance =amount-debit;
            var amount = document.getElementById('balance');
            amount.value = tot_balance;
         }

function calculateAmount(val) {
        var balance = document.getElementById("balance");
        var credit = document.getElementById("credit").value;

           var tot_amount = balance + credit;
            var amount = document.getElementById('total_balance');
            amount.value = tot_amount;
         }

</script>

@endsection