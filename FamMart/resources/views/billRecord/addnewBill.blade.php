
@extends('dashboard/pagelayout')
@section('content')
    <div class="header pb-8 pt-5 pt-md-4">
      <div class="container-fluid">
        <div class="header-body">
          <!-- Card stats -->
            <div class="row"> 
            </div>
        </div>
      </div>
    </div>

    <div class="container-fluid mt--6">
      <div class="row">
        <div class="container-fluid">
          <div class="card">
            <div class="card-header">
              <div class="row align-items-center">
               <div class="col-md-12">
                 <h3>Add Bill</h3>
                </div>
                <div class="col-md-12">
                    <form action="{{url('/bill-insert')}}" method="post">
                        {{ csrf_field() }}
                        
                        @include('includes.msg') 
                        <div class="row">
                            <div class="col-md-4 mb-3">
                            <label>Bill Name</label>
                                <input id="name" type="text" class="form-control" name="name" placeholder="Bil Name" required>
                            </div>
                            <div class="col-md-4 mb-3">
                            <label>Bill Type</label>
                            <select class="form-control" id="type" name="type" required>
                                <option value="">Select Type</option>
                                @foreach($billtype as $b)
                                    <option value="{{$b->id}}">{{$b->name}}</option>
                                @endforeach
                            </select> 
                            </div>
                            <div class="col-md-4 mb-3">
                            <label>Total Amount</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                     <span class="input-group-text">Nu.</span>
                                </div>
                                    <input id="amount" type="number" class="form-control" name="amount" placeholder="Amount" required>
                            </div>                            
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 mb-3">
                            <label>Status</label>
                            <select class="form-control" id="status" name="status" required>
                                <option value="">Select Status</option>
                                @foreach($status as $p)
                                    <option value="{{$p->id}}">{{$p->name}}</option>
                                @endforeach
                            </select> 
                            </div>
                            <div class="col-md-8 mb-3">
                            <label>Remarks</label>
                                <textarea class="form-control" id="remark" name="remark" placeholder="Remarks" required></textarea>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-success">
                                    {{ __('Submit') }}
                                </button>
                                <a class="btn btn-primary" href="{{ url('bill-list') }}"> Back</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
        
@endsection