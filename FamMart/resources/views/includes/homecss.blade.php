<!-- Font Awesome Icons -->
<link href="{{asset('vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">

<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>

<!-- Plugin CSS -->
<link href="{{asset('vendor/magnific-popup/magnific-popup.css')}}" rel="stylesheet">

<!-- Theme CSS - Includes Bootstrap -->
<link href="{{asset('css/creative.min.css')}}" rel="stylesheet">
