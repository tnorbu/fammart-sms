
 <!-- Navigation -->
 <ul class="navbar-nav mb-md-3">
          <li class="nav-item">
            <a class="nav-link " href="{{url('/dashboard')}}">
              <i class="ni ni-chart-bar-32"></i>Dashboard
            </a>
          </li>
        </ul>
        <hr class="my-2">
         <h4 class="navbar-heading text-muted">Stock Clearance</h4>
        <hr class="my-2">
        <ul class="navbar-nav mb-md-3">
          <li class="nav-item">
            <a class="nav-link " href="{{url('/stock-record')}}">
              <i class="ni ni-books"></i>Stock Entry/Update
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href="{{url('/stock-sold')}}">
              <i class="ni ni-cart"></i>Stock Sold List
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href="{{url('/stock-import')}}">
              <i class="ni ni-archive-2"></i> Stock Imported List
            </a>
          </li>
        </ul>
        <hr class="my-2">
         <h4 class="navbar-heading text-muted">Daily Sales Record</h4>
        <hr class="my-2">
      <ul class="navbar-nav mb-md-3">
        <li class="nav-item">
            <a class="nav-link" href="{{url('/debit-list')}}">
              <i class="ni ni-money-coins"></i>Debit Sales
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{url('/credit-list')}}">
              <i class="ni ni-credit-card"></i> Credit Sales
            </a>
          </li>
      </ul>
      <hr class="my-2">
         <h4 class="navbar-heading text-muted">Bill Payment Record</h4>
        <hr class="my-2">
        <ul class="navbar-nav mb-md-3">
        <li class="nav-item">
            <a class="nav-link" href="{{url('/bill-list')}}">
              <i class="ni ni-book-bookmark"></i>Bill Entry/Update
            </a>
          </li>
        <li class="nav-item">
            <a class="nav-link" href="{{url('/importbill-list')}}">
              <i class="ni ni-key-25 text-info"></i>Imported Bill Record
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href="{{url('/salebill-list')}}">
              <i class="ni ni-cart"></i>Sales Bill Record
            </a>
          </li>
       </ul>
       <hr class="my-2">
         <h4 class="navbar-heading text-muted">Master Data</h4>
        <hr class="my-2">
        <ul class="navbar-nav mb-md-3">
          <li class="nav-item">
            <a class="nav-link " href="{{url('/stock-type')}}">
              <i class="ni ni-chart-pie-35"></i> Stock Type
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{url('/stock')}}">
              <i class="ni ni-bullet-list-67"></i> Stock
            </a>
          </li>
        </ul>
        <!-- Divider -->
        <hr class="my-2">
          <h4 class="navbar-heading text-muted">User Management</h4>
        <hr class="my-2">
        <ul class="navbar-nav mb-md-3">
          <li class="nav-item">
            <a class="nav-link" href="{{url('/user-list')}}">
              <i class="ni ni-circle-08"></i> User
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>