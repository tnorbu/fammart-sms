

  <link href="./assets/img/brand/favicon.png" rel="icon" type="image/png">
  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <!-- Icons -->
  <link rel="stylesheet" href="{{asset("./assets/js/plugins/nucleo/css/nucleo.css")}}">
  <link rel="stylesheet" href="{{asset("./assets/js/plugins/@fortawesome/fontawesome-free/css/all.min.css")}}">
  <!-- CSS Files -->
  <link rel="stylesheet" href="{{asset("./assets/css/argon-dashboard.css?v=1.1.1")}}">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.21/datatables.min.css"/>


