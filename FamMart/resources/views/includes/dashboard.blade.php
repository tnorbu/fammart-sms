
<script>
var ctx = document.getElementById('dailyStats');
var dailyStats = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['Juice','Milk','Noodle','Biscuit','Dalda'],

        datasets: [{
            label: 'StockType-Debit',
            data: [{{$juicedebit}}, {{$milkdebit}}, {{$noodledebit}}, {{$biscuitdebit}},{{$daldadebit}}],
    
            backgroundColor: 'rgba(280, 99, 132, 1)',
            borderColor: [
            ],
            borderWidth: 1
        },
        {
            label: 'StockType-Credit',
            data: [{{$juicecredit}}, {{$milkcredit}}, {{$noodlecredit}}, {{$biscuitcredit}},{{$daldacredit}}],
                  
            backgroundColor:  'rgba(255, 206, 86, 0.8)',

            borderColor: [
            ],
            borderWidth: 1
        }
      ]
    },
    options: {
      scales: {
          yAxes: [{
              ticks: {
                  beginAtZero: true
              }
          }]
      },
    legend: {
    	display: true
    },
  	tooltips: {
    	callbacks: {
      	label: function(tooltipItem) {
        console.log(tooltipItem)
        	return tooltipItem.yLabel;
        }
      }
    }
  }
});

var ctx = document.getElementById('monthlyStats');
var monthlyStats = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['Juice','Milk','Noodle','Biscuit','Dalda'],

        datasets: [{
            label: 'StockType-Debit',
            data: [{{$mjuicedebit}}, {{$mmilkdebit}}, {{$mnoodledebit}}, {{$mbiscuitdebit}},{{$mdaldadebit}}],
    
            backgroundColor: 'rgba(280, 99, 132, 1)',
            borderColor: [
            ],
            borderWidth: 1
        },
        {
            label: 'StockType-Credit',
            data: [{{$mjuicecredit}}, {{$mmilkcredit}}, {{$mnoodlecredit}}, {{$mbiscuitcredit}},{{$mdaldacredit}}],
                  

            backgroundColor: 'rgba(75, 192, 192, 0.5)',
            borderColor: [
            ],
            borderWidth: 1
        }
      ]
    },
    options: {
      scales: {
          yAxes: [{
              ticks: {
                  beginAtZero: true
              }
          }]
      },
    legend: {
    	display: true
    },
  	tooltips: {
    	callbacks: {
      	label: function(tooltipItem) {
        console.log(tooltipItem)
        	return tooltipItem.yLabel;
        }
      }
    }
  }
});
var ctx = document.getElementById('overallStats');
var overallStats = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ['Juice','Milk', 'Noodle', 'Biscuit','Dalda'],
        datasets: [{
            label: 'Statistics',
            data: [{{$alljuicesale}}, {{$allmilksale}}, {{$allnoodlesale}}, {{$allbiscuitsale}},{{$alldaldasale}}],
            backgroundColor: [
                'rgba(280, 99, 132, 0.1)',
                'rgba(54, 162, 235, 0.8)',
                'rgba(255, 206, 86, 0.8)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)'

            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',

            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        },
        legend: {
        	display: false
        },
      	tooltips: {
        	callbacks: {
          	label: function(tooltipItem) {
            console.log(tooltipItem)
            	return tooltipItem.yLabel;
            }
          }
        }
    }
});



</script>