 <!-- Footer -->
 <footer class="footer">
        <div class="row align-items-center justify-content-xl-between">
          <div class="col-xl-6">
             <p class="copyright text-center text-xl-left text-muted">FAM MART</p>
          </div>
          <div class="col-xl-6">
            <ul class="nav nav-footer justify-content-center justify-content-xl-end">
              <li class="nav-item">
                <p class="nav-link" target="_blank"> All Right Reserved - 2020</p>
              </li>
            </ul>
          </div>
        </div>
  </footer>