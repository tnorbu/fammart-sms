@extends('dashboard/pagelayout')

@section('custom_css')
@include('includes/chart-css')
@endsection

@section('content')
    <div class="header pb-7 pt-5 pt-md-4">
      <div class="container-fluid">
        <div class="header-body">
        </div>
      </div>
    </div>
  <div class="container-fluid mt--5"> 
    <div class="row">
      <div class="col-md-6">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Daily Stock Sold Info</h3>
          </div>
          <div class="card-body">
            <table class="table table-bordered">
              <thead>
                <tr>
                 <th>Particulars</th>
                 <th>Debit</th>
                 <th>Credit</th>
                 <th>Total Amount</th>
                </tr>
              </thead>
              <tr>
                <td>Goods</td>
                <td>{{$dailydebit}}</td>
                <td>{{$dailycredit}}</td>
                <td>{{$dailysale}}</td>
              </tr>
            </table>           
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="card">
          <div class="card-header">
           <h3 class="card-title">Monthly Stock Sold Info</h3>
          </div>
          <div class="card-body">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>Particulars</th>
                  <th>Debit</th>
                  <th>Credit</th>
                  <th>Total Amount</th>
                </tr>
              </thead>
              <tr>
                <td>Goods</td>
                <td>{{$monthlydebit}}</td>
                <td>{{$monthlycredit}}</td>
                <td>{{$monthlysale}}</td>
              </tr>
            </table>           
          </div>
        </div>
      </div>
    </div></br>
    <div class="row">
      <div class="col-md-6">
        <div class="card card-chart">
          <div class="card-header">
           <h3 class="card-category">Daily Statistics</h3>
           <h4 class="card-title">Stock Type vs. Sale</h4>
          </div>
          <div class="card-body chart">
            <canvas id="dailyStats" height="80px"></canvas>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="card card-chart">
          <div class="card-header">
            <h3 class="card-category">Monthly Statistics</h3>
            <h4 class="card-title">Stock Type vs. Sale</h4>
          </div>
          <div class="card-body chart">
          <canvas id="monthlyStats" height="80px"></canvas>
        </div>
      </div>
    </div>
  </div></br>

    <div class="row">
      <div class="col-md-12">
        <div class="card card-chart">
          <div class="card-header">
           <h3 class="card-category">Overall Statistics</h3>
          </div>
          <div class="card-body chart">
            <canvas id="overallStats" height="80px"></canvas>
          </div>
        </div>
      </div>
    

@endsection
@section('custom_scripts')

@include('includes/chart-js')
@include('includes/dashboard')
@endsection

