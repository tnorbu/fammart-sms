<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockType extends Model
{
    protected $table = 'fammart_stock_types';
    protected $primaryKey = 'id';
    protected $fillable = ['name','submitted_by'];
    public $timestamps=false;
}
