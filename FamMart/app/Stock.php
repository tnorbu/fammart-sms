<?php

namespace App;
use App\StockType;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $table = 'fammart_stocks';
    protected $primaryKey = 'id';
    protected $fillable = ['stocktype_id','stock','submitted_by'];
    public $timestamps=false;

    public function stockType()
    {
        return $this->belongsTo(StockType::class,'stocktype_id','id');
    }
}
