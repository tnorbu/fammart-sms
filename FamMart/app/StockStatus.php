<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockStatus extends Model
{
    protected $table = 'fammart_stock_status';
    protected $primaryKey = 'id';
    protected $fillable = ['name'];
    public $timestamps=false;
}
