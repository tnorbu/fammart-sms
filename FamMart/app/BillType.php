<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillType extends Model
{
    protected $table = 'fammart_billtype';
    protected $primaryKey = 'id';
    protected $fillable = ['name'];
    public $timestamps=false;
}
