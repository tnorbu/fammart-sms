<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockHistory extends Model
{
    protected $table = 'fammart_stockhistories';
    protected $primaryKey = 'id';
    protected $fillable = ['stockrecord_id','stocktype_id','stock_id','quantity','status',
                    'rate','name','sale_rate','total_amount','date','payment_status','remarks','submitted_by'];
    public $timestamps=false;


    public function paymentStatus()
    {
        return $this->belongsTo(PaymentStatus::class,'status','id');
    }
}
