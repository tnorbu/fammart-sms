<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentStatus extends Model
{
    protected $table = 'fammart_payment_status';
    protected $primaryKey = 'id';
    protected $fillable = ['name'];
    public $timestamps=false;
}
