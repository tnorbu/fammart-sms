<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;

use App\StockType;
use App\Stock;
use App\StockStatus;
use App\PaymentStatus;
use App\User;
use App\BillType;

class MasterImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'master:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'To import all the master data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $this->importMaster("stockstatus", new StockStatus);
        $this->importMaster("paymentstatus", new PaymentStatus);
        $this->importMaster("billtype", new BillType);
        $this->importMaster("stocktype", new StockType);
        $this->importStock("stock", new Stock);
        $this->importUser("user", new User);



    }
    public function importMaster($filename, Model $model) {
        if (($handle = fopen ( public_path () . '/master/'.$filename.'.csv', 'r' )) !== FALSE) {
            $this->line("Importing ".$filename." tables...");
            $i=0;
            while ( ($data = fgetcsv ( $handle, 1000, ',' )) !== FALSE ) {
                $data = [
                    'id' => $data[0],
                    'name' => $data[1],
                ];
                 try {
                    if($model::firstOrCreate($data)) {
                        $i++;
                    }
                } catch(\Exception $e) {
                    $this->error('Something went wrong!'.$e);
                    return;

                }
            }

        fclose ( $handle );
        $this->line($i." entries successfully added in the ".$filename." table.");
    }

   }


   public function importStock($filename, Model $model) {
    if (($handle = fopen ( public_path () . '/master/'.$filename.'.csv', 'r' )) !== FALSE) {
        $this->line("Importing ".$filename." tables...");
        $i=0;
        while ( ($data = fgetcsv ( $handle, 1000, ',' )) !== FALSE ) {
            $data = [
                'id' => $data[0],
                'stocktype_id' => $data[1],
                'stock' => $data[2],
            ];
             try {
                if($model::firstOrCreate($data)) {
                    $i++;
                }
            } catch(\Exception $e) {
                $this->error('Something went wrong!'.$e);
                return;

            }
        }

    fclose ( $handle );
    $this->line($i." entries successfully added in the ".$filename." table.");
}
   }
   

public function importUser($filename, Model $model) {
    if (($handle = fopen ( public_path () . '/master/'.$filename.'.csv', 'r' )) !== FALSE) {
        $this->line("Importing ".$filename." tables...");
        $i=0;
        while ( ($data = fgetcsv ( $handle, 1000, ',' )) !== FALSE ) {
            $data = [
                'id' => $data[0],
                'name' => $data[1],
                'email' => $data[2],
                'email_verified_at' => ($data[3]=='' ? NULL:$data[3]),
                'password' => $data[4],
            ];
             try {
                if($model::firstOrCreate($data)) {
                    $i++;
                }
            } catch(\Exception $e) {
                $this->error('Something went wrong!'.$e);
                return;

            }
        }

    fclose ( $handle );
    $this->line($i." entries successfully added in the ".$filename." table.");
}
 }

}
