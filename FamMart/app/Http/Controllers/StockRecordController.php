<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StockRecord;
use App\ImportRecord;
use App\StockType;
use App\Stock;
use App\PaymentStatus;
use Auth;
use DB;
use Carbon;

class StockRecordController extends Controller
{  
    public function __construct(Request $request) {
        
        $this->request = $request;
    }
    public function stockType() {
        $id = $this->request->input('stock_types');
        //dd($id);
        $stock=DB::table('fammart_stocks')
                    ->where('stocktype_id', '=', $id)
                    ->get();
        return response()->json($stock);
    }
    
    //stock entry form
    public function stockEntry(){
      $stock_types= StockType::all();
      $stocks=Stock::all();
       return view('stockRecord.stockentry',compact('stock_types','stocks'));    
    }

    //stock store part
    public function stockStore(Request $request){
        $data = new StockRecord;
        $data->date =$now=date('Y-m-d');
        $data->supplier_name = $request->supplier_name;
        $data->stocktype_id = $request->stocktype;
        $data->stock_id= $request->stock;
        $data->quantity = $request->quantity;
        $data->rate = $request->rate;
        $data->totalAmount = $request->total_amount;
        $data->remarks = $request->remark;
        $data->status = 'S'; // stock submitted
        $data->submitted_by = Auth::user()->name;
        $data->save();

        $data = new ImportRecord;
        $data->date =$now=date('Y-m-d');
        $data->supplier_name = $request->supplier_name;
        $data->stocktype_id = $request->stocktype;
        $data->stock_id= $request->stock;
        $data->quantity = $request->quantity;
        $data->rate = $request->rate;
        $data->totalAmount = $request->total_amount;
        $data->submitted_by = Auth::user()->name;
        $data->remarks = $request->remark;
        $data->save();

         return redirect('/stock-entry')->with('success','Stock Insert Successfully!!');
    }

     //stockrecord fetching detail part
     public function stockRecord(){
        $stockrecords=StockRecord::where('quantity','>',0)->orderBy('id','DESC')->get();
        return view('stockRecord.stockRecordList',compact('stockrecords'));   
    }
    public function stockView($id){
        $stock=StockRecord::find($id);
        return view('stockRecord.stockview',compact('stock'));
    }
    public function stockEdit($id){
        $stock = StockRecord::find($id); 
        $status=PaymentStatus::all(); 
        $stock_types= StockType::all();
                 
        return view('stockRecord.stockedit',compact('stock','status','stock_types'));
    }

    public function Update(Request $request, $id){

        if(($request->input('history')) < ($request->input('quantity'))){

            return redirect()->back()->with('error', 'Taken Quantity Cannot Be Greater Than Existing Quantity');
        }else 

        $qty=floatval($request->input('history')) - floatval($request->input('quantity'));

        if($request->input('status') =='T'){
            DB::table('fammart_stockhistories')->insert([
                'stockrecord_id' =>$id,
                'stockType_id' => $request->input('stocktype'),
                'stock_id' => $request->input('stock'),
                'quantity' => $request->input('quantity'),
                'rate' => $request->input('price'),
                'status' => $request->input('status'),
                'name' => $request->input('name'),
                'sale_rate' => $request->input('rate'),
                'total_amount' => $request->input('total_amount'),
                'date'  =>$now=date('Y-m-d'),
                'payment_status' => $request->input('p_status'),
                'remarks'=>$request->input('remark'),
                'submitted_by' => Auth::user()->name,

            ]);
        }

        $data = StockRecord::find($id);
        $data->quantity = $qty;
        $data->status = $request->input('status');
        $data->remarks = $request->input('remark');
        $data->save();

        return redirect('/stock-record')->with('success','Stock Update Successfully!!');     
    }

      public function destory($id){
        $stockrecords=StockRecord::find($id)->delete();
        return redirect('/stock-record')->with('success','Stock Deleted Successfully!!');
     }

     //stock import part
     public function stockImport(){
        $stockimports=ImportRecord::orderBy('id','DESC')->get();
        return view('stockImport.stockImportList',compact('stockimports'));   
    }
    public function importDestory($id){
        $stockimports=ImportRecord::find($id)->delete();
        return redirect('/stock-import')->with('success','Stock Deleted Successfully!!');
     }
}
