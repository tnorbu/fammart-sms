<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Hash;
use Auth;

class UserController extends Controller
{
    public function indexUser(){
        $users=User::all();
        return view('acl.users',compact('users'));
    }
    public function addUser(){
        return view('acl.addUser');
    }

    public function regUser(Request $request){
        $data=new User;
        $data->name=$request->name;
        $data->email=$request->email;
        $data->password=Hash::make($request->password);
        $data->submitted_by = Auth::user()->name;

        $data->save();

        return redirect('add-user')->with('success','User Added Successfully!!');

    }

    public function userEdit($id){
        $users=User::find($id);
        return view('acl.edit',compact('users'));
    }

    public function Update(Request $request){
        $users = User::find($request->id);
        $users->name=$request->name;
        $users->email=$request->email;
        $users->password=Hash::make($request->password);
        $users->save();

        return redirect('user-list')->with('success','User Updated Successfully!!');

    }

    public function destory($id){
        $data=User::find($id)->delete();
        return back()->with('success','Deleted Successfully');
    }
}
