<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PaymentStatus;
use App\DailySaleRecord;
use App\StockHistory;
use Auth;
use Carbon\Carbon;


class DailySaleRecordController extends Controller
{
    public function saleRecord(){
        $status=PaymentStatus::all();
        $dailysale=DailySaleRecord::orderBy('id','DESC')->get();
        return view('dailysaleRecord.dailysaleRecord',compact('status','dailysale'));
    }
    public function adddailySale(){
        $status=PaymentStatus::all();
        return view('dailysaleRecord.adddailySale', compact('status'));
    }

    public function insertSale(Request $request){
        $data=new DailySaleRecord;
        $data->date=$request->date;
        $data->particular=$request->particular;
        $data->amount=$request->total_amount;
        $data->status=$request->status;
        $data->takenBy=$request->name;
        $data->remarks=$request->remark;
        $data->submitted_by = Auth::user()->name;

        $data->save();

        return redirect('add-dailySale')->with('success','DailySale Added Successfully!!');
    }

    public function destroySale($id){
        $data=DailySaleRecord::find($id)->delete();
        return back()->with('success','Deleted Successfully');
    }

    //credit part

    public function creditList(){
        $creditlist=StockHistory::where('payment_status','=',2)
                                ->whereDate('date', Carbon::today())
                                ->orderBy('id','DESC')->get();
        return view('dailysaleRecord.creditList',compact('creditlist'));
    }
    public function creditView($id){
        $creditlist=StockHistory::find($id);
        return view('dailysaleRecord.creditview',compact('creditlist'));
    }

    public function creditEdit($id){
        $status=PaymentStatus::all();
        $creditlist=StockHistory::find($id);
        return view('dailysaleRecord.creditedit',compact('status','creditlist'));
    }

    public function creditUpdate(Request $request, $id){

        $creditlist=StockHistory::find($id);
        $creditlist->amount=$request->amount;
        $creditlist->status=$request->status;
        $creditlist->remarks=$request->remark;
        $creditlist->save();

        return redirect('credit-list')->with('success','Credit Update Successfully!!');
    }

    public function destroyCredit($id){
        $data=StockHistory::find($id)->delete();
        return back()->with('success','Deleted Successfully');
    }

    //debit list
    public function debitList(){
        $debitlist=StockHistory::where('payment_status','=',1)
                                ->whereDate('date', Carbon::today())
                                ->orderBy('id','DESC')->get();;
        return view('dailysaleRecord.debitList',compact('debitlist'));
    }
    public function debitView($id){
        $debitlist=StockHistory::find($id);
        return view('dailysaleRecord.debitview',compact('debitlist'));
    }
    
    public function destroyDebit($id){
        $data=StockHistory::find($id)->delete();
        return back()->with('success','Deleted Successfully');
    }

}
