<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StockType;
use App\Stock;
use Auth;

class MasterController extends Controller
{
    public function type(){
        $stocktype=StockType::all();
        return view('master.stocktype', compact('stocktype'));
    }

    public function insertType(Request $request){
        $data = new StockType;
        $data->name = $request->stocktype;
        $data->save();

        return redirect('stock-type')->with('success','Stock Type Added Successfully!!');
    }

    public function typeEdit($id){
        $stocktype=StockType::find($id);
        return view('master.stocktypeedit', compact('stocktype'));
    }
    public function typeUpdate(Request $request, $id){
        // dd($id);
        $stocktype=StockType::find($id);
        $stocktype->name = $request->stocktype;
        $stocktype->save();

        return redirect('stock-type')->with('success','Stock Type Updated Successfully!!');
    }
    public function destory($id){
        $stocktypes=StockType::find($id)->delete();
        return back()->with('success','Deleted Successfully');
     }




    //stock part
    public function stock(){
        $stocktype=StockType::all();
        $stock=Stock::all();
        return view('master.stock',compact('stocktype','stock'));
    }
    public function insertStock(Request $request){
        $data = new Stock;
        $data->stocktype_id=$request->stocktype;
        $data->stock = $request->stock;
        $data->save();

        return redirect('stock')->with('success','Stock Added Successfully!!');
    }
    public function stockEdit($id){
        $stocktype=StockType::all();
        $stock=Stock::find($id);
        return view('master.stockedit',compact('stock','stocktype'));
    }
    public function stockUpdate(Request $request, $id){
        $stock=Stock::find($id);
        $stock->stocktype_id=$request->stocktype;
        $stock->stock = $request->stock;
        $stock->save();

        return redirect('stock')->with('success','Stock Updated Successfully!!');
    }
    public function destroy($id){
        $stocks=Stock::find($id)->delete();
        return back()->with('success','Deleted Successfully');
     }
}
