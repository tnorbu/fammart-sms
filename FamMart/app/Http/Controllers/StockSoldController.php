<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StockHistory;
use App\Stock;


class StockSoldController extends Controller
{
    public function stockSold(){
        $stockhistories = StockHistory::orderBy('id','DESC')->get();
        return view('stocksold.index',compact('stockhistories'));
    }
    public function destory($id){
        $stockhistories = StockHistory::find($id)->delete();
        return redirect('/stock-sold')->with('success','Stock Sold Deleted Successfully!!');
     }

}
