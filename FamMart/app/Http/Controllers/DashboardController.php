<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StockHistory;
use App\DailySaleRecord;
use Carbon\Carbon;
use DB;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(){
        //dailysale
        $dailydebit=DB::table('fammart_stockhistories')
                  ->where('payment_status','=',1)
                  ->whereDate('date', Carbon::today())
                  ->SUM('total_amount');
          // dd($dailydebit);
        $dailycredit=DB::table('fammart_stockhistories')
                  ->where('payment_status','=',2)
                  ->whereDate('date', Carbon::today())
                  ->SUM('total_amount');

        $dailysale=DB::table('fammart_stockhistories')
                  ->whereDate('date', Carbon::today())
                  ->SUM('total_amount');

        //Monlthly sale
        $monthlydebit=DB::table('fammart_stockhistories')
                  ->where('payment_status','=',1)
                  ->where(DB::raw('month(fammart_stockhistories.date)'), '=',date('n'))
                  ->SUM('total_amount');

        $monthlycredit=DB::table('fammart_stockhistories')
                  ->where('payment_status','=',2)
                  ->where(DB::raw('month(fammart_stockhistories.date)'), '=',date('n'))
                  ->SUM('total_amount');

        $monthlysale=DB::table('fammart_stockhistories')
                  ->where(DB::raw('month(fammart_stockhistories.date)'), '=',date('n'))
                  ->SUM('total_amount');
         // dd($monthlysale);

        
         //dailysale
       $juicedebit=DB::table('fammart_stockhistories')
                 ->where('stocktype_id','=',1)
                 ->where('payment_status','=',1)
                 ->whereDate('date', Carbon::today())
                 ->SUM('quantity');
         //dd($juicedebit);

        $juicecredit=DB::table('fammart_stockhistories')
                  ->where('stocktype_id',1)
                  ->where('payment_status','=',2)
                  ->whereDate('date', Carbon::today())
                  ->SUM('quantity');

        $milkdebit=DB::table('fammart_stockhistories')
                  ->where('stocktype_id','=',2)
                  ->where('payment_status','=',1)
                  ->whereDate('date', Carbon::today())
                  ->SUM('quantity');

        $milkcredit=DB::table('fammart_stockhistories')
                  ->where('stocktype_id','=',2)
                  ->where('payment_status','=',2)
                  ->whereDate('date', Carbon::today())
                  ->SUM('quantity');
        
        $biscuitdebit=DB::table('fammart_stockhistories')
                  ->where('stocktype_id','=',3)
                  ->where('payment_status','=',1)
                  ->whereDate('date', Carbon::today())
                  ->SUM('quantity');

        $biscuitcredit=DB::table('fammart_stockhistories')
                  ->where('stocktype_id','=',3)
                  ->where('payment_status','=',2)
                  ->whereDate('date', Carbon::today())
                  ->SUM('quantity');
    
        $noodledebit=DB::table('fammart_stockhistories')
                  ->where('stocktype_id','=',4)
                  ->where('payment_status','=',1)
                  ->whereDate('date', Carbon::today())
                  ->SUM('quantity');

        $noodlecredit=DB::table('fammart_stockhistories')
                  ->where('stocktype_id','=',4)
                  ->where('payment_status','=',2)
                  ->whereDate('date', Carbon::today())
                  ->SUM('quantity');

        $daldadebit=DB::table('fammart_stockhistories')
                  ->where('stocktype_id','=',5)
                  ->where('payment_status','=',1)
                  ->whereDate('date', Carbon::today())
                  ->SUM('quantity');

        $daldacredit=DB::table('fammart_stockhistories')
                  ->where('stocktype_id','=',5)
                  ->where('payment_status','=',2)
                  ->whereDate('date', Carbon::today())
                  ->SUM('quantity');


        
        //monlthly stock type sold
        $mjuicedebit=DB::table('fammart_stockhistories')
                  ->where('stocktype_id','=',1)
                  ->where('payment_status','=',1)
                  ->where(DB::raw('month(date)'), '=',date('n'))
                  ->SUM('quantity');
        //dd('mjuicedebit');
        $mjuicecredit=DB::table('fammart_stockhistories')
                  ->where('stocktype_id','=',1)
                  ->where('payment_status','=',2)
                  ->where(DB::raw('month(fammart_stockhistories.date)'), '=',date('n'))
                  ->SUM('quantity');

        $mmilkdebit=DB::table('fammart_stockhistories')
                  ->where('stocktype_id','=',2)
                  ->where('payment_status','=',1)
                  ->where(DB::raw('month(fammart_stockhistories.date)'), '=',date('n'))
                  ->SUM('quantity');

        $mmilkcredit=DB::table('fammart_stockhistories')
                  ->where('stocktype_id','=',2)
                  ->where('payment_status','=',2)
                  ->where(DB::raw('month(fammart_stockhistories.date)'), '=',date('n'))
                  ->SUM('quantity');
        
        $mbiscuitdebit=DB::table('fammart_stockhistories')
                  ->where('stocktype_id','=',3)
                  ->where('payment_status','=',1)
                  ->where(DB::raw('month(fammart_stockhistories.date)'), '=',date('n'))
                  ->SUM('quantity');

        $mbiscuitcredit=DB::table('fammart_stockhistories')
                  ->where('stocktype_id','=',3)
                  ->where('payment_status','=',2)
                  ->where(DB::raw('month(fammart_stockhistories.date)'), '=',date('n'))
                  ->SUM('quantity');
    
        $mnoodledebit=DB::table('fammart_stockhistories')
                  ->where('stocktype_id','=',4)
                  ->where('payment_status','=',1)
                  ->where(DB::raw('month(fammart_stockhistories.date)'), '=',date('n'))
                  ->SUM('quantity');

        $mnoodlecredit=DB::table('fammart_stockhistories')
                  ->where('stocktype_id','=',4)
                  ->where('payment_status','=',2)
                  ->where(DB::raw('month(fammart_stockhistories.date)'), '=',date('n'))
                  ->SUM('quantity');

        $mdaldadebit=DB::table('fammart_stockhistories')
                  ->where('stocktype_id','=',5)
                  ->where('payment_status','=',1)
                  ->where(DB::raw('month(fammart_stockhistories.date)'), '=',date('n'))
                  ->SUM('quantity');

        $mdaldacredit=DB::table('fammart_stockhistories')
                  ->where('stocktype_id','=',5)
                  ->where('payment_status','=',2)
                  ->where(DB::raw('month(fammart_stockhistories.date)'), '=',date('n'))
                  ->SUM('quantity');


        //overall stock type sold
        
        $alljuicesale=DB::table('fammart_stockhistories')
                  ->where('stocktype_id','=',1)
                  ->SUM('quantity');

         // dd('$alljuicesale');
        
        $allmilksale=DB::table('fammart_stockhistories')
                  ->where('stocktype_id','=',2)
                  ->SUM('quantity');

        
        $allbiscuitsale=DB::table('fammart_stockhistories')
                  ->where('stocktype_id','=',3)
                  ->SUM('quantity');

        
        $allnoodlesale=DB::table('fammart_stockhistories')
                  ->where('stocktype_id','=',4)
                  ->SUM('quantity');

        
        $alldaldasale=DB::table('fammart_stockhistories')
                  ->where('stocktype_id','=',5)
                  ->SUM('quantity');
        
        return view('dashboardlist.stockinfo',compact(
            'dailydebit','dailycredit','dailysale',
            'monthlydebit','monthlycredit','monthlysale',

            'juicedebit','juicecredit','milkdebit','milkcredit',
            'biscuitdebit','biscuitcredit','noodledebit','noodlecredit',
            'daldadebit','daldacredit',
            
            'mjuicedebit','mjuicecredit','mmilkdebit','mmilkcredit',
            'mbiscuitdebit','mbiscuitcredit','mnoodledebit','mnoodlecredit',
            'mdaldadebit','mdaldacredit',
            
            'alljuicesale','allmilksale','allbiscuitsale','allnoodlesale','alldaldasale',
        ));


    }
}