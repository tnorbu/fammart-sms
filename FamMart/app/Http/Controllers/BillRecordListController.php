<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PaymentStatus;
use App\BillRecord;
use App\BillType;
use App\BillHistory;
use Auth;
use DB;
use Carbon;

class BillRecordListController extends Controller
{
    public function billRecord(){
        $bill=BillRecord::where('status','=',2)
                     ->orderBy('id','DESC')->get();
        return view('billRecord.billrecordList',compact('bill'));
    }

    public function addBill(){
        $status=PaymentStatus::all();
        $billtype=BillType::all();
        return view('billRecord.addnewBill',compact('status','billtype'));
    }

    public function record(Request $request){
        $data= new BillRecord;
        $data->date =$now=date('Y-m-d');
        $data->billName=$request->name;
        $data->billtypeId=$request->type;
        $data->totalAmount=$request->amount;
        $data->status=$request->status;
        $data->billTrans = 'S'; 
        $data->remarks=$request->remark;
        $data->submittedBy = Auth::user()->name;

        $data->save();

        return redirect('bill-record')->with('success','Bill Added Successfully!!');

    }

    public function billEdit($id){
        $status=PaymentStatus::all();
        $bill=BillRecord::find($id);
        return view('billRecord.billedit',compact('status','bill'));
    }

    public function Update(Request $request, $id){
        $data = BillHistory::find($id);
        if($request->input('billTrans') =='T'){
            DB::table('fammart_billhistories')->insert([
                'billId' =>$id,
                'billName' => $request->input('name'),
                'billTypeId' => $request->input('bill'),
                'debit' => $request->input('balance'),
                'credit' => $request->input('credit'),
                'status' => $request->input('status'),
                'billTrans'=>$request->input('billTrans'),
                'totalAmount' => $request->input('totalamount'),
                'date'  =>$now=date('Y-m-d'),
                'remarks'=>$request->input('remark'),
                'submittedBy' => Auth::user()->name,

            ]);
        }
        $data = BillRecord::find($id); 
        $data->date =$now=date('Y-m-d');
        $data->totalAmount = $request->input('totalamount');
        $data->status = $request->input('status');
        $data->remarks = $request->input('remark');
        $data->save();

        return redirect('bill-list')->with('success','Bill Updated Successfully!!');
    }

    public function destory($id){
        $data=BillRecord::find($id)->delete();
        return back()->with('success','Deleted Successfully');
    }


    //sale Bill record
    public function salebillRecord(){
        $status=BillHistory::where('billTypeId','=','Sales Bill')
                        ->orderBy('id','DESC')->get();
        return view('salesBill.billrecordList',compact('status'));
    }
//sale Bill record
public function importbillRecord(){
    $status=BillHistory::where('billTypeId','=','Imported Bill')
                    ->orderBy('id','DESC')->get();
    return view('salesBill.importedbillrecord',compact('status'));
}
public function saledestory($id){
    $data=BillHistory::find($id)->delete();
    return back()->with('success','Deleted Successfully');
}


}
