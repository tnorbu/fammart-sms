<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillRecord extends Model
{
    protected $table = 'fammart_bill_records';
    protected $primaryKey = 'id';
    protected $fillable = ['date','billName','billtypeId','totalAmount','status','billTrans','remarks','submittedBy'];
    public $timestamps=false;

    public function paymentStatus()
    {
        return $this->belongsTo(PaymentStatus::class,'status','id');
    }
    public function billType()
    {
        return $this->belongsTo(BillType::class,'billTypeId','id');
    }

}
