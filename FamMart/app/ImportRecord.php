<?php

namespace App;

use App\StockRecord;
use App\StockType;

use Illuminate\Database\Eloquent\Model;

class ImportRecord extends Model
{
    protected $table = 'fammart_stockimports';
    protected $primaryKey = 'id';
    protected $fillable = ['date','supplier_name','stocktype_id','stock_id','quantity','rate','totalAmount','submitted_by','remarks'];
    public $timestamps=false;


    public function stock()
    {
        return $this->belongsTo(Stock::class,'stock_id','id');
    }
    public function stockType()
    {
        return $this->belongsTo(StockType::class,'stocktype_id','id');
    }
}
