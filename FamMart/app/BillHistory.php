<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillHistory extends Model
{
    protected $table = 'fammart_billhistories';
    protected $primaryKey = 'id';
    protected $fillable = ['billId','date','billTrans','billName','billTypeId','debit','credit','totalAmount',
          'status','remarks','submittedBy'];
    public $timestamps=false;


    public function paymentStatus()
    {
        return $this->belongsTo(PaymentStatus::class,'status','id');
    }
}
