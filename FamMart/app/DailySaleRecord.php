<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DailySaleRecord extends Model
{
    protected $table = 'fammart_dailysale_records';
    protected $primaryKey = 'id';
    protected $fillable = ['date','particular','amount','status','takenBy','remarks','submitted_by'];
    public $timestamps=false;

    public function paymentStatus()
    {
        return $this->belongsTo(PaymentStatus::class,'status','id');
    }
}


