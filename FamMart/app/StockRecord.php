<?php

namespace App;
use App\StockRecord;
use App\StockType;
// use App\StockTransaction;

use Illuminate\Database\Eloquent\Model;

class StockRecord extends Model
{
    protected $table = 'fammart_stockrecords';
    protected $primaryKey = 'id';
    protected $fillable = ['date','supplier_name','stocktype_id','stock_id','quantity','status','rate','totalAmount','submitted_by','remarks'];
    public $timestamps=false;


    public function stock()
    {
        return $this->belongsTo(Stock::class,'stock_id','id');
    }
    public function stockType()
    {
        return $this->belongsTo(StockType::class,'stocktype_id','id');
    }
    // public function stockTransaction(){
    //     return $this->belongsTo(StockTransaction::class,'stockNumber','stockNumber');

    // }
}
