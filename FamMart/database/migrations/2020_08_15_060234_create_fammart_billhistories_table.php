<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFammartBillhistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fammart_billhistories', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->string('billId');
            $table->string('billName');
            $table->string('billTypeId');
            $table->string('debit');
            $table->string('credit');
            $table->integer('totalAmount');
            $table->string('status');
            $table->string('billTrans');
            $table->string('remarks');
            $table->string('submittedBy');
            $table->timestamps();
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fammart_billhistories');
    }
}
