<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFammartStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fammart_stocks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('stocktype_id')->unsigned();
            $table->foreign('stocktype_id')->constrained()->references('id')->on('fammart_stock_types')->onDelete('cascade');
            $table->string('stock');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fammart_stocks');
    }
}
