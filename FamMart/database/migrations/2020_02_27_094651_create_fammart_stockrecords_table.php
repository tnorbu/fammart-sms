<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFammartStockrecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fammart_stockrecords', function (Blueprint $table) {
            $table->increments('id');
            $table->string('date');
            $table->string('supplier_name');
            $table->char('status');
            $table->string('stocktype_id');
            $table->string('stock_id');
            $table->string('quantity');
            $table->string('rate');
            $table->string('totalAmount');
            $table->string('remarks')->nullable();
            $table->string('submitted_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fammart_stockrecords');
    }
}
