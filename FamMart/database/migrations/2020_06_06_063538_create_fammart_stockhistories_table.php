<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFammartStockhistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fammart_stockhistories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('stockrecord_id');
            $table->char('status');
            $table->string('stocktype_id');
            $table->string('stock_id');
            $table->string('quantity');
            $table->string('rate');
            $table->string('name');
            $table->string('sale_rate');
            $table->string('total_amount');
            $table->date('date');
            $table->string('payment_status');
            $table->string('remarks');
            $table->string('submitted_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fammart_stockhistories');
    }
}
