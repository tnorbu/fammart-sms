<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFammartBillRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fammart_bill_records', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->string('billName');
            $table->string('billTypeId');
            $table->integer('totalAmount');
            $table->string('status');
            $table->string('billTrans');
            $table->string('remarks');
            $table->string('submittedBy');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fammart_bill_records');
    }
}
