<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'IndexController@home');
//Login or logout
Route::get('login', 'AuthController@loginForm')->name('login');
Route::post('login', 'AuthController@login');
Route::post('logout', 'AuthController@logout')->name('logout');
Route::get('logout', 'AuthController@logout');

Route::group(['middleware' => 'auth'], function () {
//dashboard route
Route::get('/dashboard', 'DashboardController@index')->name('index');


//master data route
Route::get('/stock-type', 'MasterController@type');
Route::post('/add-stocktype', 'MasterController@insertType');
Route::get('/stocktype-edit/{id}', 'MasterController@typeEdit');
Route::post('/stocktype-update/{id}', 'MasterController@typeUpdate');
Route::get('/stocktype-delete/{id}', 'MasterController@destory');

//stock
Route::get('/stock', 'MasterController@stock');
Route::post('/add-stock', 'MasterController@insertStock');
Route::get('/stock-edit/{id}', 'MasterController@stockEdit');
Route::post('/stock-update/{id}', 'MasterController@stockUpdate');
Route::get('/stock-delete/{id}', 'MasterController@destroy');


//Stock Record
Route::get('/stock-entry', 'StockRecordController@stockEntry');
Route::post('/stock-insert', 'StockRecordController@stockStore');

//stock import
Route::get('/stock-import', 'StockRecordController@stockImport');
Route::get('/stockimport-delete/{id}','StockRecordController@ImportDestory');

//json
Route::get('/json-stock-type','StockRecordController@stockType');

//stock submitted part
Route::get('/stock-record', 'StockRecordController@stockRecord');
Route::get('/stock-view/{id}', 'StockRecordController@stockView');
Route::get('/stockrecord-edit/{id}', 'StockRecordController@stockEdit');
Route::post('/update-stock/{id}','StockRecordController@Update')->name('update-stock');
Route::get('/stockrecord-delete/{id}','StockRecordController@destory');


//stock-sold
Route::get('/stock-sold', 'StockSoldController@stockSold');
Route::get('/stocksold-delete/{id}','StockSoldController@destory');



//Imported Bill Record
Route::get('/bill-list', 'BillRecordListController@billRecord')->name('bill-list');
Route::get('/bill-record', 'BillRecordListController@addBill');
Route::post('/bill-insert', 'BillRecordListController@record');
Route::get('/bill-edit/{id}', 'BillRecordListController@billEdit');
Route::post('/bill-update/{id}', 'BillRecordListController@Update');
Route::get('/bill-delete/{id}', 'BillRecordListController@destory');

///Sale Bill Record
Route::get('/salebill-list', 'BillRecordListController@salebillRecord');
Route::get('/importbill-list', 'BillRecordListController@importbillRecord');
Route::get('/salebill-delete/{id}', 'BillRecordListController@saledestory');



//DailySaleRecord
Route::get('/daily-saleRecord', 'DailySaleRecordController@saleRecord');
Route::get('/add-dailySale', 'DailySaleRecordController@adddailySale');
Route::post('/insert-dailysale', 'DailySaleRecordController@insertSale');
Route::get('/saleRecord-delete/{id}','DailySaleRecordController@destroySale');

//credit list
Route::get('/credit-list', 'DailySaleRecordController@creditList');
Route::get('/credit-view/{id}', 'DailySaleRecordController@creditView');
Route::get('/credit-delete/{id}','DailySaleRecordController@destroyCredit');

//debit list
Route::get('/debit-list', 'DailySaleRecordController@debitList');
Route::get('/debit-view/{id}', 'DailySaleRecordController@debitView');
Route::get('/debit-delete/{id}','DailySaleRecordController@destroyDebit');


//user route
Route::get('/user-list', 'UserController@indexUser');
Route::get('/add-user', 'UserController@addUser');
Route::post('/user-add', 'UserController@regUser');
Route::get('/user-edit/{id}', 'UserController@userEdit');
Route::post('/user-update/{id}', 'UserController@Update');
Route::get('/user-delete/{id}', 'UserController@destory');

});






